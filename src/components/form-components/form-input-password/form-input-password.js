import React, {Component} from 'react';

import "./form-input-password.css";

class FormInputPassword extends Component {

    state = {
        hidden: true,
        password: ""
    };

    onShowClick = () => {
        this.setState({hidden: !this.state.hidden});
    };

    handlePasswordChange = (e) => {
        this.setState({password: e.target.value});
    };

    render() {
        const {label, name, placeholder, type} = this.props;
        const {password, hidden} = this.state;

        const inputType = (hidden)? "password" : "text";

        return (
            <div className="field form-input">
                <label>{label}</label>
                <input
                    type={inputType}
                    name={name}
                    placeholder={placeholder}
                    onChange={this.handlePasswordChange}
                    value={password}
                    pattern="^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9]).{8,16}$"
                />
                <i className="eye icon form-input-password-icon" onClick={this.onShowClick}/>
            </div>
        );
    }
}

export default FormInputPassword;