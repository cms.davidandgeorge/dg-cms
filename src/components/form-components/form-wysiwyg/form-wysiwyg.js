import React, {Component} from "react";
import CKEditor from "@ckeditor/ckeditor5-react";
import ClassicEditor from "@ckeditor/ckeditor5-build-classic";

import "./form-wysiwyg.css";

class FormWysiwyg extends Component {
    render() {

        const {label} = this.props;

        return (
            <div className="field">
                <label>{label}</label>
                <CKEditor
                    editor={ ClassicEditor }
                    data=""
                />
            </div>
        );
    }
}

export default FormWysiwyg;