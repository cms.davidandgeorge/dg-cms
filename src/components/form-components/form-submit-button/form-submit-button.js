import React from "react";

import "./form-submit.css";

const FormSubmitButton = ({text}) => {
    return (
        <div className="form-submit-button">
            <button className="fluid ui button teal">{text}</button>
        </div>
    );
};

export default FormSubmitButton;