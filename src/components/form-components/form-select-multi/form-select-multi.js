import React from 'react'

import "./form-select-multi.css";

import { Dropdown } from 'semantic-ui-react'

const options = [
    { key: 'peter', text: 'Luke', value: 'peter' },
    { key: 'css', text: 'Leia', value: 'css' },
    { key: 'design', text: 'Han', value: 'design' },
    { key: 'ember', text: 'Chewie', value: 'ember' },
];

const FormSelectMulti = ({label, placeholder}) => (
        <div className="field">
            <label>{label}</label>
            <Dropdown
                placeholder={placeholder}
                fluid
                multiple
                selection
                options={options}
            />
        </div>
);

export default FormSelectMulti;
