import React, {Component} from 'react';

import "./form-input.css";

class FormInput extends Component {
    render() {

        const {label, name, placeholder, type} = this.props;

        return (
            <div className="field form-input">
                <label>{label}</label>
                <input type={type} name={name} placeholder={placeholder}/>
            </div>
        );
    }
}

export default FormInput;