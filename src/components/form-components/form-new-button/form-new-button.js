import React from 'react';

import "./form-new-button.css"
import {Link} from "react-router-dom";

const FormNewButton = ({link, title}) => {
    return (
        <Link to={link}>
            <button className="fluid ui button teal">
                {title}
            </button>
        </Link>
    );
};

export default FormNewButton;
