import React from 'react'
import { Dropdown } from 'semantic-ui-react'

const countryOptions = [
    { key: 'af', value: 'af',  text: 'Frodo' },
    { key: 'ax', value: 'ax',  text: 'Sam' },
    { key: 'al', value: 'al',  text: 'Pippin' },
    { key: 'dz', value: 'dz',  text: 'Merry' },
    { key: 'as', value: 'as',  text: 'Boromir' },
    { key: 'ad', value: 'ad',  text: 'Aragorn' },
    { key: 'ao', value: 'ao',  text: 'Gimli' },
    { key: 'ai', value: 'ai',  text: 'Legolas' },
    { key: 'ag', value: 'ag',  text: 'Gandalf' },

];


const FormSelect = ({label, placeholder}) => (
    <div className="field">
        <label>{label}</label>
        <Dropdown
            placeholder={placeholder}
            fluid
            search
            selection
            options={countryOptions}
        />
    </div>
);

export default FormSelect;

