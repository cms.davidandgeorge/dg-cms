import React, {Component} from 'react';

import "./single-input-form.css"

class SingleInputForm extends Component {

    render() {

        const {value,label} = this.props;

        return (
            <div className="single-input-form">
                <p>{label}</p>
                <form className="ui action fluid input" >
                    <input type="text" value={value}/>
                    <button className="ui button">Change</button>
                </form>
            </div>

        );
    }
}

export default SingleInputForm;