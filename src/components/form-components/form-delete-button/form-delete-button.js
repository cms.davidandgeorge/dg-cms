import React from 'react';

import "./form-delete-button.css";

import {Link} from "react-router-dom";

const FormDeleteButton = ({title, link}) => {
    return (
        <Link to={link}>
            <button className="ui red button">{title}</button>
        </Link>
    )
};

export default FormDeleteButton;
