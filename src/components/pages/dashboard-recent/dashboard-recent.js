import React, {Component} from 'react';

import "./dashboard-recent.css";

import DashboardHeader from "../../layout-components/dashboard-header/dashboard-header";
import DashboardProjectGrid from "../../layout-components/dashboard-project-grid/dashboard-project-grid";


class DashboardRecent extends Component {
    render() {
        return (
            <div className="dashboard ui container">
                <DashboardHeader/>
                <DashboardProjectGrid />
            </div>
        );
    }
}

export default DashboardRecent;