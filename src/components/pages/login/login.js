import React from 'react';

// Components Imports
import LoginForm from "../../layout-components/login-form/login-form";

// Styles
import "./login.css";

const Login = () => {
    return (
        <div className="login">
            <div>
                <h1 className="login-amp">&</h1>
                <h2 className="login-text">Projects Log In</h2>
                <LoginForm/>
            </div>
        </div>
    );
};

export default Login;
