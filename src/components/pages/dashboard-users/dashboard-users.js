import React, {Component} from 'react';

import "./dashboard-users.css";

import DashboardHeader from "../../layout-components/dashboard-header/dashboard-header";
import DashboardUsersTable from "../../layout-components/dashboard-users-table/dashboard-users-table";

class DashboardUsers extends Component {
    render() {
        return (
            <div className="dashboard ui container">
                <DashboardHeader/>
                <div className="ui grid">
                    <DashboardUsersTable
                        header="Employees"
                        button="New Employee + "
                        formLink={"/dashboard/users/create-employee"}
                    />
                    <DashboardUsersTable
                        header="Clients"
                        button="New Client + "
                        formLink={"/dashboard/users/create-client"}/>
                </div>
            </div>
        );
    }
}

export default DashboardUsers;