import React, {Component} from 'react';

import "./project-overview.css"
import proposal from "../../../assets/images/Proposal and details.png";
import wireframes from "../../../assets/images/Wireframes.png";

import ProjectOverviewCard from "../../layout-components/project-overview-card/project-overview-card";
import ProjectOverviewHeader from "../../layout-components/project-overview-header/project-overview-header";
import SingleInputForm from "../../form-components/single-input-form/single-input-form";
import FormHeader from "../../layout-components/form-header/form-header";
import ProjectOverviewComment from "../../layout-components/project-overview-comment/project-overview-comment";
import FormWysiwyg from "../../form-components/form-wysiwyg/form-wysiwyg";
import FormSubmitButton from "../../form-components/form-submit-button/form-submit-button";

class ProjectOverview extends Component {
    render() {
        return (
            <div className="ui container form-container">
                <div className="project-overview-container">
                    <FormHeader title="Project Overview" url="/dashboard/recent"/>
                </div>
                <ProjectOverviewHeader
                    title="Porter's Gin"
                    phone="07914244865"
                    email= "ben@portersgin.co.uk"
                    website="portersgin.co.uk"
                />
                <div className="form-container project-overview-container-bottom">
                    <SingleInputForm value="www.google.com" label="Content folder"/>
                    <SingleInputForm value="www.google.com" label="Content folder"/>
                    <SingleInputForm value="www.google.com" label="Content folder"/>
                </div>
                <div className="project-overview-container-bottom">
                    <div className="ui grid">
                            <ProjectOverviewCard title="Proposal & Details" img={proposal}/>
                            <ProjectOverviewCard title="Wireframes" img={wireframes}/>
                    </div>
                </div>
                <h3>Internal Comments</h3>
                <div className="project-overview-container-bottom">
                    <ProjectOverviewComment
                    text="Increase Padding"
                    date={"21/09/2019"}
                    />
                </div>
                <form className="ui form" action="">
                    <FormWysiwyg label={"New Note"}/>
                    <FormSubmitButton
                        text="Add Note"
                    />
                </form>
        </div>
        );
    }
}

export default ProjectOverview;