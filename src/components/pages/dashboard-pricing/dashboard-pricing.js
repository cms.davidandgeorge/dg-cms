import React, {Component} from 'react';

import "./dashboard-pricing.css";

import DashboardHeader from "../../layout-components/dashboard-header/dashboard-header";
import DashboardPricingTable from "../../layout-components/dashboard-pricing-table/dashboard-pricing-table";

class DashboardPricing extends Component {

    render() {

        return (
            <div className="dashboard ui container">
                <DashboardHeader/>
                <div className="ui grid">
                    <DashboardPricingTable
                        header="Packages"
                        button="New Package + "
                        formLink="/dashboard/pricing/create-package"
                    />
                    <DashboardPricingTable
                        header="Services"
                        button="New Package + "
                        formLink="/dashboard/pricing/create-service"
                    />
                </div>
            </div>
        )
    }
}


export default DashboardPricing;