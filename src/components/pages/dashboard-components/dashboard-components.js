import React, {Component} from 'react';

import "./dashboard-components.css";

import DashboardHeader from "../../layout-components/dashboard-header/dashboard-header";
import DashboardComponentsTable from "../../layout-components/dashboard-components-table/dashboard-components-table";


class DashboardComponents extends Component {
    render() {
        return (
            <div className="dashboard ui container">
                <DashboardHeader/>
                <div className="ui grid">
                    <DashboardComponentsTable header="Default Components" button="New Default Component + " formLink="/create-default-component"/>
                    <DashboardComponentsTable header="Custom Components" button="New Custom Component + " formLink="/create-custom-component"/>
                </div>
            </div>
        );
    }
}

export default DashboardComponents;