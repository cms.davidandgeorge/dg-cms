import React, {Component} from 'react';

import{Route, BrowserRouter} from 'react-router-dom';

import "./app.css";

import Login from "./pages/login/login";
import DashboardRecent from "./pages/dashboard-recent/dashboard-recent";
import CreateProject from "./pages-forms/create-project/create-project";
import DashboardUsers from "./pages/dashboard-users/dashboard-users";
import CreateComponent from "./pages-forms/create-component/create-component";
import DashboardComponents from "./pages/dashboard-components/dashboard-components";
import CreateClient from "./pages-forms/create-client/create-client";
import CreateEmployee from "./pages-forms/create-employee/create-employee";
import DashboardPricing from "./pages/dashboard-pricing/dashboard-pricing";
import CreateService from "./pages-forms/create-service/create-service";
import CreatePackage from "./pages-forms/create-package/create-package";
import ProjectOverview from "./pages/project-overview/project-overview";

class App extends Component {
    render() {
        return (
            <div>
                <BrowserRouter>
                    <Route exact path="/" component={Login} />

                    <Route exact path="/dashboard/recent" component={DashboardRecent} />
                    <Route exact path="/dashboard/recent/create-project" component={CreateProject} />
                    <Route exact path="/dashboard/recent/edit-project" component={CreateProject} />
                    <Route exact path="/dashboard/recent/archive-project" component={CreateProject} />

                    <Route exact path="/dashboard/project-overview" component={ProjectOverview} />

                    <Route exact path="/dashboard/archived" component={DashboardRecent} />
                    <Route exact path="/dashboard/archived/edit-project" component={CreateProject} />
                    <Route exact path="/dashboard/archived/delete-project" component={CreateProject} />

                    <Route exact path="/dashboard/users" component={DashboardUsers} />
                    <Route exact path="/dashboard/users/create-employee" component={CreateEmployee} />
                    <Route exact path="/dashboard/users/create-client" component={CreateClient} />

                    <Route exact path="/dashboard/components" component={DashboardComponents} />
                    <Route exact path="/dashboard/components/create-custom-component" component={CreateComponent} />

                    <Route exact path="/dashboard/pricing" component={DashboardPricing} />
                    <Route exact path="/dashboard/pricing/create-service" component={CreateService} />
                    <Route exact path="/dashboard/pricing/create-package" component={CreatePackage} />
                </BrowserRouter>
            </div>
        );
    }
}

export default App;