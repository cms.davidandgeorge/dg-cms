import React, {Component} from 'react';

import "./dashboard-project-card.css"

import {Link} from "react-router-dom";
import DashboardProjectCardMenu from "../dashboard-project-card-menu/dashboard-project-card-menu";

class DashboardProjectCard extends Component {
    render() {
        return (
            <div className="column four wide">
                <div className="dashboard-project-card-padding">
                    <div className="ui card dashboard-project-card-shadow">
                        <Link to={"/dashboard/project-overview"}>
                            <div className="content dashboard-project-card">
                                <div className="dashboard-project-card-title">Bravo Whisky Golf</div>
                            </div>
                        </Link>
                        <div className="extra content dashboard-project-new-card-bottom">
                            Created 21/09/19
                            <DashboardProjectCardMenu/>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default DashboardProjectCard;