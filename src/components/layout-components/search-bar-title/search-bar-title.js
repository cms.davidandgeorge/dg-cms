import React from 'react';

import "./search-bar-title.css";

import {Link} from "react-router-dom";

const SearchBarTitle = () => {
    return (
        <Link to="/dashboard/recent">
            <div className="ui four wide search-bar-title-logo">
                <span className="search-bar-title-amp">&</span>
                <span className="search-bar-title-text">David & George</span>
            </div>
        </Link>
    );
};

export default SearchBarTitle;
