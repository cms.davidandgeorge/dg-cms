import React, {Component} from 'react';

import "./dashboard-navbar.css";

import {NavLink} from "react-router-dom";

class DashboardNavbar extends Component {
    render() {
        return (
            <div className="ui five item menu dashboard-navbar">
                <NavLink
                    className="item"
                    to="/dashboard/recent"
                    activeClassName="active">
                    Recent Projects
                </NavLink>
                <NavLink
                    className="item"
                    to="/dashboard/archived"
                    activeClassName="active">
                    ArchivedProjects
                </NavLink>
                <NavLink
                    className="item"
                    to="/dashboard/users"
                    activeClassName="active">
                    Users
                </NavLink>
                <NavLink
                    className="item"
                    to="/dashboard/components"
                    activeClassName="active">
                    Components
                </NavLink>
                <NavLink
                    className="item"
                    to="/dashboard/pricing"
                    activeClassName="active">
                    Pricing
                </NavLink>
            </div>
        );
    }
}

export default DashboardNavbar;