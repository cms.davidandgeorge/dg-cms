import React, {Component} from 'react';

import "./dashboard-users-table.css"

import FormNewButton from "../../form-components/form-new-button/form-new-button";
import FormEditButton from "../../form-components/form-edit-button/form-edit-button";
import FormDeleteButton from "../../form-components/form-delete-button/form-delete-button";

class DashboardUsersTable extends Component {
    render() {

        // Props
        const {header, button, formLink} = this.props;

        return (
            <div className="column eight wide">
                <h3>{header}</h3>
                <FormNewButton title={button} link={formLink}/>
                <table class="ui striped table dashboard-table">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>E-mail</th>
                            <th>Actions</th>
                        </tr>
                    </thead>

                    <tbody>
                        <tr>
                            <td>Cornelius Gobblestick</td>
                            <td>cornelius.gobblestick@yahoo.com</td>
                            <td>
                                <FormEditButton link={"a"} title="Edit"/>
                                <FormDeleteButton link={"a"} title="Delete"/>
                            </td>
                        </tr>
                        <tr>
                            <td>Jamie Harington</td>
                            <td>jamieharingonton@yahoo.com</td>
                            <td>
                                <FormEditButton link={"a"} title="Edit"/>
                                <FormDeleteButton link={"a"} title="Delete"/>
                            </td>
                        </tr>
                        <tr>
                            <td>Jill Lewis</td>
                            <td>jilsewris22@yahoo.com</td>
                            <td>
                                <FormEditButton link={"a"} title="Edit"/>
                                <FormDeleteButton link={"a"} title="Delete"/>
                            </td>
                        </tr>
                        <tr>
                            <td>John Lilki</td>
                            <td>jhlilk22@yahoo.com</td>
                            <td>
                                <FormEditButton link={"a"} title="Edit"/>
                                <FormDeleteButton link={"a"} title="Delete"/>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        );
    }
}

export default DashboardUsersTable;