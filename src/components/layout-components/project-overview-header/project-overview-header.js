import React from 'react';

import "./project-overview-header.css";

import FormHeader from "../form-header/form-header";

const ProjectOverviewHeader = ({title, phone, email, website}) => {
    return (
        <div className="ui list">
            <div className="item project-overview-header-company">{title}</div>
            <div className="item project-overview-header-item">{phone}</div>
            <div className="item project-overview-header-item">{email}</div>
            <div className="item project-overview-header-item">{website}</div>
        </div>
    );
};

export default ProjectOverviewHeader;
