import React, {Component} from 'react';

import "./dashboard-search-bar.css";

import SearchBarTitle from "../search-bar-title/search-bar-title";

class DashboardSearchBar extends Component {
    render() {
        return (
            <div className="ui grid search-bar-flex">
                <SearchBarTitle/>
                <div className="column twelve wide">
                    <div className="ui search">
                        <div className="ui icon input search-bar-container ">
                            <input className="prompt search-bar" type="text" placeholder="Search"/>
                            <i className="search icon"/>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default DashboardSearchBar;