import React from 'react';

import "./dashboard-project-card-menu.css";

import { Dropdown } from 'semantic-ui-react';


const DashboardProjectCardMenu = () => (
    <Dropdown text='' icon='ellipsis horizontal' className='dashboard-project-card-menu-icon'>
        <Dropdown.Menu>
            <Dropdown.Item icon='edit' text='Edit' />
            <Dropdown.Item icon='archive' text='Archive' />
        </Dropdown.Menu>
    </Dropdown>
);

export default DashboardProjectCardMenu;