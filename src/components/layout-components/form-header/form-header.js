import React from 'react';

import "./form-header.css";

import {Link} from "react-router-dom";

const FormHeader = ({url,title}) => {
    return (
        <div>
            <div className="form-header-back">
                <Link to={url}><i className="angle left icon"/>Back to Dashboard</Link>
            </div>
            <h1><span className="form-header-amp">& </span>{title}</h1>
            <br/>
        </div>
    );
};

export default FormHeader;
