import React, {Component} from 'react';

import "./dashboard-components-table.css";

import FormEditButton from "../../form-components/form-edit-button/form-edit-button";
import FormDeleteButton from "../../form-components/form-delete-button/form-delete-button";
import FormNewButton from "../../form-components/form-new-button/form-new-button";

class DashboardComponentsTable extends Component {

    render() {

        const {header, button, formLink} = this.props;

        return (
            <div className="column eight wide">
                <h3>{header}</h3>
                <FormNewButton title={button} link={formLink}/>
                <table className="ui striped table dashboard-table">
                    <thead>
                    <tr>
                        <th>Title</th>
                        <th>Actions</th>
                    </tr>
                    </thead>

                    <tbody>
                    <tr>
                        <td>Navigation</td>
                        <td>
                            <FormEditButton link={"a"} title="Edit"/>
                            <FormDeleteButton link={"a"} title="Delete"/>
                        </td>
                    </tr>
                    <tr>
                        <td>Footer</td>
                        <td>
                            <FormEditButton link={"a"} title="Edit"/>
                            <FormDeleteButton link={"a"} title="Delete"/>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        );
    }
}

export default DashboardComponentsTable;