import React, {Component} from 'react';

import "./dashboard-pricing-table.css";

import FormEditButton from "../../form-components/form-edit-button/form-edit-button";
import FormDeleteButton from "../../form-components/form-delete-button/form-delete-button";
import FormNewButton from "../../form-components/form-new-button/form-new-button";

class DashboardPricingTable extends Component {

    render() {

        const {header, formLink, button} = this.props;

        return (
            <div className="column eight wide">
                <h3>{header}</h3>
                <FormNewButton title={button} link={formLink}/>
                <table class="ui striped table dashboard-table">
                    <thead>
                    <tr>
                        <th>Service</th>
                        <th>Price</th>
                        <th>Actions</th>
                    </tr>
                    </thead>

                    <tbody>
                    <tr>
                        <td>Basic Website</td>
                        <td>£999</td>
                        <td>
                            <FormEditButton link={"a"} title="Edit"/>
                            <FormDeleteButton link={"a"} title="Delete"/>
                        </td>
                    </tr>
                    <tr>
                        <td>Custom Design Website</td>
                        <td>£1.199</td>
                        <td>
                            <FormEditButton link={"a"} title="Edit"/>
                            <FormDeleteButton link={"a"} title="Delete"/>
                        </td>
                    </tr>
                    <tr>
                        <td>Deluxe Website</td>
                        <td>£1.999</td>
                        <td>
                            <FormEditButton link={"a"} title="Edit"/>
                            <FormDeleteButton link={"a"} title="Delete"/>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        );
    }
}

export default DashboardPricingTable;