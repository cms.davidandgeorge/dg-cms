import React from 'react';

import "./dashboard-header.css";

import DashboardSearchBar from "../dashboard-search-bar/dashboard-search-bar";
import DashboardNavbar from "../dashboard-navbar/dashboard-navbar";

const DashboardHeader = () => {
    return (
        <div>
            <DashboardSearchBar />
            <DashboardNavbar />
        </div>
    );
};

export default DashboardHeader;
