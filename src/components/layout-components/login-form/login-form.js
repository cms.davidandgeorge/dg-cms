import React from 'react';

import "./login-form.css"

import {Redirect} from "react-router-dom";



class LoginForm extends React.Component {

    state = {
        redirect: false,
    };


    onSubmit = event => {
        event.preventDefault();
        this.setState({redirect:true})
    };

    render() {

        const {redirect} = this.state;

        if(redirect) {
            return <Redirect to="/dashboard/recent"/>
        }

        return (
            <div className="login-form">
                <form action="" onSubmit={this.onSubmit}>
                    <input className="login-form-input" type="email" placeholder="Email"/>
                    <input className="login-form-input" type="password" placeholder="Password"/>
                    <button className="ui button secondary login-form-button">Log In</button>
                </form>
            </div>
        );
    }


}

export default LoginForm;
