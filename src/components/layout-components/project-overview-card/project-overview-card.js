import React, {Component} from 'react';

import "./project-overview-card.css";

import {Link} from "react-router-dom";

class ProjectOverviewCard extends Component {
    render() {

        const {link, title,img} = this.props;

        return (
            <div className="eight wide column">
                <Link to={link}>
                    <div className="ui card project-overview-card-shadow">
                        <div className="content project-overview-card">
                            <img
                                src={img}
                                alt="Blah"
                                height="120px"
                            />
                            <h3 className="project-overview-card-title">{title}</h3>
                        </div>
                    </div>
                </Link>
            </div>
        );
    }
}

export default ProjectOverviewCard;