import React, {Component} from 'react';

import "./dashboard-project-grid.css";

import DashboardProjectNew from "../dashboard-project-card-new/dashboard-project-new";
import DashboardProjectCard from "../dashboard-project-card/dashboard-project-card";

class DashboardProjectGrid extends Component {
    render() {
        return (
            <div className="ui grid">
                <DashboardProjectNew />
                <DashboardProjectCard />
                <DashboardProjectCard />
                <DashboardProjectCard />
                <DashboardProjectCard />
                <DashboardProjectCard />
                <DashboardProjectCard />
            </div>
        );
    }
}

export default DashboardProjectGrid;