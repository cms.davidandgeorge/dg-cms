import React, {Component} from 'react';

import "./create-package.css";

import FormInput from "../../form-components/form-input/form-input";
import FormSubmitButton from "../../form-components/form-submit-button/form-submit-button";
import FormHeader from "../../layout-components/form-header/form-header";

class CreatePackage extends Component {
    render() {
        return (
            <div className="ui container create-employee">
                <FormHeader url="/dashboard/recent" title="New Package"/>
                <form action="" className="ui form">
                    <FormInput
                        label="Package"
                        name="name"
                        type="text"
                        placeholder="Name of the package"
                    />
                    <FormInput
                        label="Price (£)"
                        name="price"
                        type="number"
                        placeholder="999"
                    />
                    <FormSubmitButton
                        text="Create Package"
                    />
                </form>
            </div>
        );
    }
}

export default CreatePackage;