import React, {Component} from 'react';

import "./create-employee.css" ;

import FormInput from "../../form-components/form-input/form-input";
import FormInputPassword from "../../form-components/form-input-password/form-input-password";
import FormSubmitButton from "../../form-components/form-submit-button/form-submit-button";
import FormHeader from "../../layout-components/form-header/form-header";


class CreateEmployee extends Component {
    render() {
        return (
            <div className="ui container form-container">
                <FormHeader url="/dashboard/recent" title="New Employee"/>
                <form action="" className="ui form">
                    <FormInput
                        label="Name"
                        name="name"
                        type="text"
                        placeholder="First and Last Name..."
                    />
                    <FormInput
                        label="Email"
                        name="name"
                        type="email"
                        placeholder="name@domain.com"
                    />
                    <FormInputPassword
                        label="Password"
                        name="name"
                        placeholder="Password"
                    />
                    <FormSubmitButton
                        text="Create Employee"
                    />
                </form>
            </div>
        );
    }
}

export default CreateEmployee;