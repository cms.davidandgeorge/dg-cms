import React, {Component} from 'react';

import FormInput from "../../form-components/form-input/form-input";
import FormSubmitButton from "../../form-components/form-submit-button/form-submit-button";
import DropdownExampleMultipleSelection from "../../form-components/form-select-multi/form-select-multi";
import DropdownExampleSearchSelection from "../../form-components/form-select/form-select";
import FormWysiwyg from "../../form-components/form-wysiwyg/form-wysiwyg";
import FormHeader from "../../layout-components/form-header/form-header";

class CreateProject extends Component {
    render() {
        return (
            <div className="ui container form-container">
                <FormHeader url="/dashboard/recent" title="New Project"/>
                <form action="" className="ui form">
                    <FormInput
                        label="Title"
                        name="title"
                        type="text"
                        placeholder="Project's name"
                    />
                    <FormInput
                        label="Who is preparing the presentation?"
                        name="name"
                        type="text"
                        placeholder="Name"
                    />
                    <DropdownExampleMultipleSelection
                        label="Assign one or more employees"
                        placeholder="Employees"
                    />
                    <DropdownExampleSearchSelection
                        label="Select package"
                        placeholder="Packages"
                    />
                    <DropdownExampleMultipleSelection
                        label="Select service/s"
                        placeholder="Services"
                    />
                    <DropdownExampleSearchSelection
                        label="Select Client"
                        placeholder="Clients"
                    />
                    <FormInput
                        label="Name/Company"
                        name="clientName"
                        type="text"
                        placeholder="Client's or Company's name"
                    />
                    <FormInput
                        label="Client's email"
                        name="clientEmail"
                        type="email"
                        placeholder="name@domain.com"
                    />
                    <FormInput
                        label="Client's Phone Number"
                        name="clientPhone"
                        type="text"
                        placeholder="name@domain.com"
                    />
                    <FormInput
                        label="Google Drive Link"
                        name="clientGoogleDrive"
                        type="text"
                        placeholder="name@domain.com"
                    />
                    <FormWysiwyg label="Other Links"/>
                    <FormWysiwyg label="Notes"/>
                    <FormSubmitButton
                        text="Create Project"
                    />
                </form>
            </div>
        );
    }
}

export default CreateProject;