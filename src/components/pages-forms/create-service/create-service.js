import React, {Component} from 'react';

import "./create-service.css";

import FormInput from "../../form-components/form-input/form-input";
import FormSubmitButton from "../../form-components/form-submit-button/form-submit-button";
import FormHeader from "../../layout-components/form-header/form-header";

class CreateService extends Component {
    render() {
        return (
            <div className="ui container form-container">
                <FormHeader url="/dashboard/recent" title="Service"/>
                <br/>
                <form action="" className="ui form">
                    <FormInput
                        label="Service"
                        name="name"
                        type="text"
                        placeholder="Name of the package"
                    />
                    <FormInput
                        label="Price (£/h)"
                        name="price"
                        type="number"
                        placeholder="99"
                    />
                    <FormSubmitButton
                        text="Create Package"
                    />
                </form>
            </div>
        );
    }
}

export default CreateService;