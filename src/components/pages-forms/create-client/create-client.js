import React, {Component} from 'react';

import "./create-client.css" ;

import FormInput from "../../form-components/form-input/form-input";
import FormInputPassword from "../../form-components/form-input-password/form-input-password";
import FormSubmitButton from "../../form-components/form-submit-button/form-submit-button";
import FormHeader from "../../layout-components/form-header/form-header";

class CreateClient extends Component {
    render() {
        return (
            <div className="ui container form-container">
                <FormHeader url="/dashboard/recent" title="New Client"/>
                <form action="" className="ui form">
                    <FormInput
                        label="Name/Company "
                        name="name"
                        type="text"
                        placeholder="Client's or Company's name"
                    />
                    <FormInput
                        label="Email"
                        name="name"
                        type="email"
                        placeholder="name@domain.com"
                    />
                    <FormInputPassword
                        label="Password"
                        name="password"
                        placeholder="Password"
                    />
                    <FormSubmitButton
                        text="Create Client"
                    />
                </form>
            </div>
        );
    }
}

export default CreateClient;